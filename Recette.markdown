FRAMACAKE v. 1.0
======================

_par ViolaineM_

Cette recette est placée sous [licence CC -By](https://creativecommons.org/licenses/by/2.0/fr/). Elle permet de produire un gâteau au yaourt (base) avec très peu de matériel et de manière très rapide. Il facilite le hacking et (donc) la créativité, tout en restant bon marché.

   * _Recette pour 6 personnes_
   * _Niveau : très facile_

Le matériel
------------

   * 1 [moule à manqué](http://fr.wikipedia.org/wiki/Moule_%C3%A0_manqu%C3%A9),
   * 1 [fouet](http://fr.wikipedia.org/wiki/Fouet_%28cuisine%29),
   * 1 pot de [yaourt](http://fr.wikipedia.org/wiki/Yaourt) vide ( servira à la mesure),
   * 1 [saladier](http://fr.wikipedia.org/wiki/Saladier).

Ingrédients
-------------

   * 1 yaourt (et son pot),
   * 2 œufs,
   * 1 pot de sucre,
   * 3 pots de farine (ou 2 pots de farine + 1 pot de fécule de maïs),
   * 1/2 pot d'huile,
   * 1/2 sachet de levure chimique,
   * 1 noix de beurre pour le moule.

__Hacking (suggestions)__

Il est possible d'ajouter d'autres ingrédients suivant vos goûts. Voici quelques exemples&nbsp;:

   * une orange non traitée, pour son jus et son zeste,
   * un citron non traité, pour son jus et son zeste,
   * une cuillère à soupe d'eau de fleur d'oranger,
   * quelques cuillières de chocolat en poudre (+ 1 cuillière à  soupe d'eau pour ne pas assécher la pâte),
   * un mélange de morceaux de pomme et de noix,
   * quelques fruits confits en petit morceaux,
   * etc.

Étape 1
-------------

   * Préchauffez le four à 180 degrés,
   * dans un saladier, battre les oeufs entiers avec le yaourt,
   * nettoyez le pot vide et séchez-le,
   * ajoutez un pot de sucre au mélange yaourt/oeufs,
   * bien mélanger,
   * ajoutez 2 pots de farine petit à petit tout en mélangeant.

__Hacking (suggestions)__

Si vous voulez utiliser un arôme liquide (jus de citron, jus d'orange, eau de fleur d'oranger, etc) ajoutez-le maintenant. Attention : maximum 2 cuillières à soupe de jus&nbsp;! Mélangez encore.


Étape 2
----------------

   * Ajoutez 1 pot de farine (ou de fécule de maïs),
   * ajoutez la levure puis l'huile.

__Hacking (suggestions)__

Ajoutez maintenant le zeste d'orange ou de citron, ou les fruits confits, ou les morceaux de pomme, ou le chocolat, etc. ou rien du tout :)

Étape 3
-------------

   * Beurrer le moule et le [chemiser](http://fr.wiktionary.org/wiki/chemiser)
   * Verser la pâte
   * Enfourner pour 40 minutes environ (moins longtemps si vous utilisez la chaleur tournante)

_Note : le gâteau est cuit lorsqu'une lame de couteau plantée en ressort sèche._

__--> Dégustez !__
